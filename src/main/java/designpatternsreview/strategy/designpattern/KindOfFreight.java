package designpatternsreview.strategy.designpattern;

public enum KindOfFreight {
	
	NORMAL {
		@Override
		public Freight getFreight() {
			return new Normal();
		}
	},
	EXPRESS {
		@Override
		public Freight getFreight() {
			return new Express();
		}
	};

	public abstract Freight getFreight();
	
}
