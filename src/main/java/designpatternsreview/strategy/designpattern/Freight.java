package designpatternsreview.strategy.designpattern;

public interface Freight {

	public double calculateFreight(int distance);
}
