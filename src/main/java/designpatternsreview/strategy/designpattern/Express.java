package designpatternsreview.strategy.designpattern;

public class Express implements Freight{

	public double calculateFreight(int distance) {
		return distance * 1.45 + 12;
	}

}
