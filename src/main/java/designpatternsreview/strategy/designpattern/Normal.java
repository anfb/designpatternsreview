package designpatternsreview.strategy.designpattern;

public class Normal implements Freight {

	public double calculateFreight(int distance) {
		return distance * 1.25 + 10;
	}
}
