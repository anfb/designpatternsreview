package designpatternsreview.app;

import java.util.Scanner;

import designpatternsreview.strategy.designpattern.Freight;
import designpatternsreview.strategy.designpattern.KindOfFreight;

public class App {

	public static void main(String[] args) {
		try {
			Scanner enter = new Scanner(System.in);
			System.out.print("Please, enter the distance: ");
			int distance = enter.nextInt();
			System.out.print("What kind of freight? \n 1 - Normal, 2 - Express: ");
			int optionFreight = enter.nextInt();
			KindOfFreight kindFreight = KindOfFreight.values()[optionFreight-1];
			
			Freight freight = kindFreight.getFreight();
			double price = freight.calculateFreight(distance);
			System.out.println(price);
		} catch (Exception e) {
			System.out.println("Calculate Freight fail.");
		}
	}

}
